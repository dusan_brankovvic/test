<style>
    table.content
    {
        width: 100%;
        border-collapse: collapse;
        padding: 0px;
        margin: 0px;
        font-family: Arial;
        font-size: 11px;
        line-height: 1.4;
    }

    .content th
    {
        border: solid 1px #000000;
        text-align: left;
    }
    .content td
    {
        border: solid 1px #000000;
        text-align: left;
    }
    .content .row
    {
        border-top: solid 1px #000000;
    }
</style>
<table class="content">
    <tr class="row">
        <th>User ID</th>
        <th>Full Name</th>
        <th>House ID</th>
        <th>Property type</th>
        <th>Full address</th>
        <th>Number of likes given</th>
        <th>Like IDs</th>
        <th>Number of likes received</th>
        <th>Number of matches</th>
        <th>Number of people</th>
        <th>Number of old men</th>
    </tr>
    @foreach ($data as $info)
        <tr>
            <td>{{ $info['userId'] }}</td>
            <td>{{ $info['fullName'] }}</td>
            <td>{{ $info['houseId'] }}</td>
            <td>{{ $info['propertyType'] }}</td>
            <td>{{ $info['fullAddress'] }}</td>
            <td>{{ $info['likesCount'] }}</td>
            <td>{{ $info['likeIds'] }}</td>
            <td>{{ $info['likesReceived'] }}</td>
            <td>{{ $info['chatMatch'] }}</td>
            <td>{{ $info['peopleCount'] }}</td>
            <td>{{ $info['oldMenCount'] }}</td>
        </tr>
    @endforeach
</table>
