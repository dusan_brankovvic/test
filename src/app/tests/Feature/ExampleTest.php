<?php

namespace Tests\Feature;

use App\Postcode;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testItShouldReturnGroupedPostCodes()
    {
        $response = $this->json('GET', 'api/postcodes');

        $response->assertStatus(200)
            ->assertJsonStructure(['data']);

        $data = json_decode($response->getContent(), true);

        $this->assertCount(10, $data['data']);
    }

    public function testItShouldReturnResponseWithoutFilter()
    {
        $postCode = Postcode::first();

        $response = $this->json('GET', 'api/locations/'. $postCode->getAttribute('id'));

        $response->assertStatus(200)
            ->assertJsonStructure(['school', 'bus', 'address']);

        $data = json_decode($response->getContent(), true);

        $this->assertCount(15, $data['school']['data']);

        $this->assertCount(5, $data['bus']);

        $this->assertCount(0, $data['address']['data']);
    }

    public function testItShouldReturnResponseWithFilterForSchool()
    {
        $postCode = Postcode::first();

        $response = $this->json('GET', 'api/locations/'. $postCode->getAttribute('id') . '/school');

        $response->assertStatus(200)
            ->assertJsonStructure(['data']);

        $data = json_decode($response->getContent(), true);

        $this->assertCount(15, $data['data']);
    }

    public function testItShouldReturnResponseWithFilterForBus()
    {
        $postCode = Postcode::first();

        $response = $this->json('GET', 'api/locations/'. $postCode->getAttribute('id') . '/bus');

        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $this->assertCount(5, $data);
    }

    public function testItShouldReturnResponseWithFilterForAddress()
    {
        $postCode = Postcode::first();

        $response = $this->json('GET', 'api/locations/'. $postCode->getAttribute('id') . '/address');

        $response->assertStatus(200)
            ->assertJsonStructure(['data']);

        $data = json_decode($response->getContent(), true);

        $this->assertCount(0, $data['data']);
    }

    public function testItShouldReturnReportWithoutPdf()
    {
        $response = $this->json('GET', 'api/report');

        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey("userId", $data[0]);
        $this->assertArrayHasKey("fullName", $data[0]);
        $this->assertArrayHasKey("houseId", $data[0]);
        $this->assertArrayHasKey("propertyType", $data[0]);
        $this->assertArrayHasKey("fullAddress", $data[0]);
        $this->assertArrayHasKey("likesCount", $data[0]);
        $this->assertArrayHasKey("likeIds", $data[0]);
        $this->assertArrayHasKey("likesReceived", $data[0]);
        $this->assertArrayHasKey("chatMatch", $data[0]);
        $this->assertArrayHasKey("peopleCount", $data[0]);
        $this->assertArrayHasKey("oldMenCount", $data[0]);
    }

    public function testItShouldReturnPdfReport()
    {
        $response = $this->json('GET', 'api/report/pdf');

        $response->assertStatus(200)->assertHeader('content-type', 'application/pdf');
    }

}
