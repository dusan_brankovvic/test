<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Chat extends Model
{

    protected $guarded = ['id'];

    public function getTotalNumberOfConversations($userId)
    {
        $sql =" SELECT COUNT(1) as chats
FROM (SELECT DISTINCT chats.from, chats.to
      FROM chats
      WHERE chats.from = $userId OR chats.to = $userId) as M";

        return DB::select( DB::raw($sql));
    }
}
