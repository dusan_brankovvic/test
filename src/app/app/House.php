<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{

    protected $guarded = ['id'];

    public function address()
    {
        return $this->hasOne(Addresse::class, 'id', 'address_id');
    }

    public function postCode()
    {
        return $this->hasOne(Postcode::class, 'id', 'postcode_id');
    }
}
