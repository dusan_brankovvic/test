<?php

namespace App\Http\Controllers;

use App\Addresse;
use App\Chat;
use App\House;
use App\Likes;
use App\Postcode;
use App\School;
use App\User;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TestController extends Controller
{
    public function postcodes(Request  $request)
    {
        $limit = 11; // To to Query, limit needs to be plus 1
        $page = ($request->has('page')) ? (int)$request->get('page') : 1;

        $offset = ($page - 1) * $limit;

        $data = DB::table('postcodes')
            ->select(DB::raw("SUBSTRING(GROUP_CONCAT(DISTINCT `postcode` SEPARATOR ';' LIMIT {$limit} OFFSET {$offset}),10) as postcodes"))
            ->get();

        return response()->json(['data'=>explode(';', $data->toArray()[0]->postcodes)]);
    }

    public function getLocations($postcodeId, $filter = null, Postcode $postcode, School  $school, Addresse  $addresse)
    {

        // get all postcodes in 10 mile radius
        $postcode = $postcode->find($postcodeId);

        if ($postcode === null) {
            throw new NotFoundHttpException('Postcode id is not found!');
        }

        $postCodesInTenMilesRange = $this->getPointsInRadius('postcodes', $postcode->getAttribute('latitude'),
            $postcode->getAttribute('longitude'));

        $response['school'] = $school->whereIn('postcode_id', array_column($postCodesInTenMilesRange, 'id'))->paginate();

        $response['bus'] =  $this->getPointsInRadius('busstops', $postcode->getAttribute('latitude'),
            $postcode->getAttribute('longitude'), null, 5, 0, 'lat', 'lon');

        $response['address'] = $addresse->whereIn('postcode_id', array_column($postCodesInTenMilesRange, 'id'))->paginate();

        if ($filter === null) {
            return response()->json($response);
        }

        if($filter !== null && !empty($response[$filter])) {
            return response()->json($response[$filter]);
        }

        return response()->json(['message'=>'Filter not found', 404]);
    }

    public function getReport($isPdf = null, User $user, House $house, Likes  $likes, Chat  $chat, PDF  $PDF)
    {
        $users = $user->take(5)->with('house')->get();

        $data = [];

        foreach ($users as $index=>$user) {
            $data[$index]['userId'] = $user->getAttribute('id');
            $data[$index]['fullName'] = $user->getAttribute('name') . " " . $user->getAttribute('surname');
            $house = $user->house()->first();
            $data[$index]['houseId'] = $house->getAttribute('id');
            switch ($house->getAttribute('propertytype')) {
                case 0:
                    $data[$index]['propertyType'] = "-";
                    break;
                case 1:
                    $data[$index]['propertyType'] = "FLAT";
                    break;
                case 2:
                    $data[$index]['propertyType'] = "small house";
                    break;
                case 3:
                    $data[$index]['propertyType'] = "big house";
                    break;
                case 4:
                    $data[$index]['propertyType'] = "Villa";
                    break;
            }
            $postcode = $house->postCode()->first()->getAttribute('postcode');
            $address = $house->address()->first()->toArray();
            unset($address['postcode_id']);
            unset($address['id']);
            $data[$index]['fullAddress'] = $postcode . " " .trim(implode(' ', $address));
            $likesData = $likes->where('a', $user->getAttribute('id'))->where('like', 1)->get();
            $data[$index]['likesCount'] = $likesData->count();
            $data[$index]['likeIds'] = $likesData->implode('id', ',');
            $data[$index]['likesReceived'] = $likes->where('b', $user->getAttribute('id'))->where('like', 1)->count();
            $data[$index]['chatMatch'] = $chat->getTotalNumberOfConversations($user->getAttribute('id'))[0]->chats;
            $data[$index]['peopleCount'] = $user->people->count();
            $data[$index]['oldMenCount'] = $user->people()->where('age', '>=', 45)->count();
        }
        if ($isPdf !== null) {
            $pdf = $PDF->loadView('report', ['data' => $data]);
            return $pdf->download('report.pdf');
        }

        return response()->json($data);
    }

    /**
     * @param $tableName
     * @param $latitude
     * @param $longitude
     * @param $distance in kilometers
     */
    public function getPointsInRadius(
        $tableName,
        $latitude,
        $longitude,
        $distance = 17,
        $limit = null,
        $offset = 0,
        $latitudeName = 'latitude',
        $longitudeName = 'longitude')
    {
        $sql = "SELECT m.*,
    p.distance_unit
             * DEGREES(ACOS(COS(RADIANS(p.latpoint))
             * COS(RADIANS(m.{$latitudeName}))
             * COS(RADIANS(p.longpoint) - RADIANS(m.{$longitudeName}))
             + SIN(RADIANS(p.latpoint))
             * SIN(RADIANS(m.{$latitudeName})))) AS distance_in_km
FROM {$tableName} AS m
JOIN (
      SELECT {$latitude} AS latpoint, {$longitude} AS longpoint,
             50.0 AS radius, 111.045 AS distance_unit
     ) AS p ON 1=1
WHERE m.{$latitudeName}
BETWEEN p.latpoint  - (p.radius / p.distance_unit)
    AND p.latpoint  + (p.radius / p.distance_unit)
    AND m.{$longitudeName} BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
    AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))";

        if ($distance !== null) {
            $sql .= " HAVING distance_in_km < {$distance}";
        }

        $sql .= " ORDER BY distance_in_km";

        if ($limit !== null) {
            $sql .= " LIMIT {$limit} OFFSET {$offset}";
        }

        $results = DB::select( DB::raw($sql));

        return $results;
    }
}
